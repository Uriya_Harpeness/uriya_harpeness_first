#pragma comment (lib, "Ws2_32.lib")

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <windows.h>
#include <thread>

#include <string.h>
#include <iostream>
#include <vector>

bool a[10][2];
std::thread t[10];

void inita()
{
	int i, j;
	for (i = 0; i < 10; i++)
	{
		for (j = 0; j < 2; j++)
		{
			a[i][j] = false;
		}
	}
}

bool exit()
{
	for (int i = 0; i < 10; i++)
	{
		if (a[i][1])
		{
			std::cout << "EXIT" << std::endl;
			return true;
		}
	}
	return false;
}

int getThread()
{
	int i = 0;
	while (a[i][0])
	{
		i++;
		if (i > 9)
		{
			return (-1);
		}
	}
	return (i);
}

void replay(SOCKET soc, short int threadNumber)
{
	char* replay = new char;

	std::cout << "Started talking with thread " <<
	threadNumber << std::endl;
	recv(soc, replay, 1024, 0);
	std::cout << "Got \"" << replay << "\" from client" << std::endl;
	std::cout << "Sending \"Accepted\"" << std::endl;
	a[threadNumber][1] = (replay == "exit");
	send(soc, "Accepted", 1024, 0);

	closesocket(soc);
	WSACleanup();

	a[threadNumber][0] = false;
	std::cout << "Ended talking with thread " <<
	threadNumber << std::endl;
	std::cout << "Port " <<	threadNumber + 4682 <<
	" is now aveliable" << std::endl;
}

int main()
{
	WSADATA info;
	SOCKET soc, newsocket;
	struct sockaddr_in server, client;
	int startError, bindError;
	int port;
	int c;
	int threadNumber;

	system("netsh interface ip show addresses \"Wi-Fi 2\"");
	inita();

	do
	{
		threadNumber = getThread();
		a[threadNumber][0] = true;
		port = 4682 + threadNumber;

		std::cout << "Waiting on port " << port <<
		", thread number " << threadNumber << std::endl;

		startError = WSAStartup(MAKEWORD(2, 0), &info);
		if (startError != 0)
		{
			printf("error... %d", GetLastError());
			system("pause");
			return 1;
		}
		soc = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (soc == INVALID_SOCKET)
		{
			printf("error... %d", GetLastError());
			system("pause");
			return 1;
		}

		server.sin_family = AF_INET;
		server.sin_addr.s_addr = INADDR_ANY;
		server.sin_port = htons(port);

		bindError = bind(soc, (struct sockaddr*) &server, sizeof(client));
		if (bindError == SOCKET_ERROR)
		{
			printf("error... %d", GetLastError());
			system("pause");
			return 1;
		}

		if (listen(soc, SOMAXCONN) == SOCKET_ERROR)
		{
			printf("error... (listen) %d", GetLastError());
			system("pause");
			return 1;
		}

		std::cout << "Finally waiting..." << std::endl;
		c = sizeof(struct sockaddr_in);
		newsocket = accept(soc, (struct sockaddr*) &client, &c);
		
		t[threadNumber] = std::thread(replay, newsocket, threadNumber);
	}
	while (!exit());

	for (int i = 0; i < 10; i++)
	{
		if (a[i][0])
		{
			t[i].join();
		}
	}

	closesocket(soc);
	closesocket(newsocket);
	WSACleanup();

	system("pause");
	return 0;
}