#pragma comment (lib, "Ws2_32.lib")

#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <windows.h>
#include <string.h>

#include <iostream>
#include <string>

using namespace std;

int main()
{
	WSADATA info;
	int err;
	SOCKET s;
	struct sockaddr_in clientService;
	string host;

	int port;

	err = WSAStartup(MAKEWORD(2,0), &info);

	cout << "Enter the IP and port number" << endl;
	cin >> host;
	cin >> port;

	if (err != 0)
	{
		printf("error... %d", GetLastError());
		system("pause");
		return 1;
	}

	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s == INVALID_SOCKET)
	{
		printf("error... %d", GetLastError());
		system("pause");
		return 1;
	}

	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr(host.c_str());
	clientService.sin_port = htons(port);
	cout << "connecting..." << endl;
	if (connect(s, (struct sockaddr*) &clientService, sizeof(clientService)) == SOCKET_ERROR)
	{
		printf("error... %d", GetLastError());
		system("pause");
		return 1;
	}

	char a[1024];
	printf("%c so far so good %c\n",1,2);
	cin >> a;
	send(s, a, 1024, 0);
	cout << "sending \"" << a << "\"" << endl;

	recv(s, a, 1024, 0);

	cout << "recived \"" << a << "\"" << endl;
	
	err = closesocket(s);
	if (err)
	{
		printf("error... %d", GetLastError());
	}
	WSACleanup();
	system("pause");
	return 0;
}