#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <windows.h>
#include <string.h>

#include <iostream>
#include <string>

using namespace std;

int main()
{
	WSADATA info;
	int err;
	SOCKET s;
	struct sockaddr_in clientService;
	string host = "10.0.0.7";

	int port = 4862;

	err = WSAStartup(MAKEWORD(2,0), &info);

	if (err != 0)
	{
		printf("error... %d", GetLastError());
		system("pause");
		return 1;
	}

	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s == INVALID_SOCKET)
	{
		printf("error... %d", GetLastError());
		system("pause");
		return 1;
	}

	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr(host.c_str());
	clientService.sin_port = htons(port);
	cout << "connecting..." << endl;
	if (connect(s, (struct sockaddr*) &clientService, sizeof(clientService)) == SOCKET_ERROR)
	{
		printf("error... %d", GetLastError());
		system("pause");
		return 1;
	}

	char a[1024];
	printf("%c so far so good %c\n",1,2);
	recv(s, a, 1024, 0);
	cout << a << endl;
	
	err = closesocket(s); //there's some problem in here :(
	if (err)
	{
		printf("error... %d", GetLastError());
	}
	WSACleanup();
	system("pause");
	return 0;
}