#include <stdio.h>
#include <stdlib.h>
#include <winsock2.h>
#include <windows.h>

#include <string.h>
#include <iostream>

using namespace std;

int main()
{

	WSADATA info;
	SOCKET soc, newsocket;
	struct sockaddr_in server, client;
	int startError, bindError;
	int port = 4862;
	int c;
	string s = "Accepted";

	startError = WSAStartup(MAKEWORD(2, 0), &info);
	if (startError != 0)
	{
		printf("error... %d", GetLastError());
		system("pause");
		return 1;
	}
	soc = socket(AF_INET, SOCK_STREAM, 0);
	if (soc == INVALID_SOCKET)
	{
		printf("error... %d", GetLastError());
		system("pause");
		return 1;
	}

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(port);

	bindError = bind(soc, (struct sockaddr*) &server, sizeof(server));
	if (bindError == SOCKET_ERROR)
	{
		printf("error... %d", GetLastError());
		system("pause");
		return 1;
	}

	if (listen(soc, SOMAXCONN) == SOCKET_ERROR)
	{
		printf("error... (listen) %d", GetLastError());
		system("pause");
		return 1;
	}

	printf("Waiting...\n");
	c = sizeof(struct sockaddr_in);
	newsocket = accept(soc, (struct sockaddr*) &client, &c);
	send(newsocket, s.c_str(), 1024, 0);
	closesocket(soc);
	closesocket(newsocket);
	WSACleanup();

	system("pause");
	return 0;
}